import pytest
import requests
from random import randint
from http import HTTPStatus

bear = 'http://0.0.0.0:5000/bear'

ok = HTTPStatus.OK

not_found = HTTPStatus.NOT_FOUND

created = HTTPStatus.CREATED


@pytest.mark.parametrize("test_input,expected", [('http://0.0.0.0:5000/info', ok), (bear, ok),
                                                 ('http://0.0.0.0:5000/bear_id', not_found)])
def test_get(test_input, expected):
    response = requests.get(test_input)
    assert response.status_code == expected


@pytest.mark.parametrize("test_input,expected",
                         [(f'{bear}/{randint(0, 1000)}', not_found),
                          (f'{bear}/{randint(0, 1000)}', not_found),
                          (f'{bear}/{randint(0, 1000)}', not_found)])
def test_get_non_exist(test_input, expected):
    response = requests.get(test_input)
    assert response.status_code == expected
    response = requests.delete(test_input)
    assert response.status_code == expected


@pytest.mark.parametrize("test_input,test_input1,expected,expected1,expected2",
                         [(bear, {"bear_type": "POLAR", "bear_name": "MISHA", "bear_age": 13.0}, created,
                           {"bear_id": 1, "bear_type": "POLAR", "bear_name": "MISHA", "bear_age": 13.0}, ok),
                          (bear, {"bear_type": "BLACK", "bear_name": "MISHA", "bear_age": 13.0}, created,
                           {"bear_id": 1, "bear_type": "BLACK", "bear_name": "MISHA", "bear_age": 13.0}, ok),
                          pytest.param(bear, {"bear_type": "RAINBOW", "bear_name": "MISHA", "bear_age": 13.0}, created,
                                       {"bear_id": 1, "bear_type": "RAINBOW", "bear_name": "MISHA", "bear_age": 13.0},
                                       ok, marks=pytest.mark.xfail),
                          (bear, {"bear_type": "BROWN", "bear_name": "MISHA", "bear_age": 13.0}, created,
                           {"bear_id": 1, "bear_type": "BROWN", "bear_name": "MISHA", "bear_age": 13.0}, ok),
                          (bear, {"bear_type": "GUMMY", "bear_name": "MISHA", "bear_age": 13.0}, created,
                           {"bear_id": 1, "bear_type": "GUMMY", "bear_name": "MISHA", "bear_age": 13.0}, ok)])
def test_post(test_input, test_input1, expected, expected1, expected2):
    response = requests.post(url=test_input, json=test_input1)
    assert response.status_code == expected
    response = requests.get(url=test_input)
    assert response.json() == [expected1]
    response = requests.delete(url=test_input)
    assert response.status_code == expected2
    response = requests.post(url='http://0.0.0.0:5000/clear')
    assert response.status_code == expected2


@pytest.mark.parametrize("test_input, test_input1, test_input2, expected, expected1, expected2, expected3",
                         [(bear, {"bear_type": "POLAR", "bear_name": "MISHA", "bear_age": 13.0},
                           {"bear_type": "BLACK", "bear_name": "MISHA", "bear_age": 13.0},
                           created, {"bear_id": 1, "bear_type": "POLAR", "bear_name": "MISHA", "bear_age": 13.0}, ok,
                           {"bear_id": 1, "bear_type": "BLACK", "bear_name": "MISHA", "bear_age": 13.0}),
                          (bear, {"bear_type": "BLACK", "bear_name": "MISHA", "bear_age": 13.0},
                           {"bear_type": "BLACK", "bear_name": "MICHAEL", "bear_age": 13.0},
                           created, {"bear_id": 1, "bear_type": "BLACK", "bear_name": "MISHA", "bear_age": 13.0}, ok,
                           {"bear_id": 1, "bear_type": "BLACK", "bear_name": "MICHAEL", "bear_age": 13.0}),
                          (bear, {"bear_type": "BROWN", "bear_name": "MISHA", "bear_age": 13.0},
                           {"bear_type": "BROWN", "bear_name": "MISHA", "bear_age": 13.0},
                           created, {"bear_id": 1, "bear_type": "BROWN", "bear_name": "MISHA", "bear_age": 13.0}, ok,
                           {"bear_id": 1, "bear_type": "BROWN", "bear_name": "MISHA", "bear_age": 13.0})])
def test_put(test_input, test_input1, test_input2, expected, expected1, expected2, expected3):
    response = requests.post(url=test_input, json=test_input1)
    assert response.status_code == expected
    response = requests.get(url=test_input)
    assert response.json() == [expected1]
    requests.put(url=f'{bear}/1', json=test_input2)
    response = requests.get(url=f'{bear}/1')
    assert response.json() == expected3
    response = requests.delete(url=f'{bear}/1')
    assert response.status_code == expected2
    response = requests.post(url='http://0.0.0.0:5000/clear')
    assert response.status_code == expected2


@pytest.mark.parametrize("test_input,expected",
                         [(f'{bear}/100', not_found), (f'{bear}/200', not_found),
                          (f'{bear}/300', not_found)])
def test_get_bear_deleted(test_input, expected):
    response = requests.get(test_input)
    assert response.status_code == expected

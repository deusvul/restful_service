from flask import Flask, jsonify
from flask import abort
from flask import request
from http import HTTPStatus

app = Flask(__name__)

info = 'Welcome to Alaska! This is CRUD service for bears in alaska. CRUD routes presented with REST naming ' \
       'notation: POST /bear - create, GET /bear - read all bears, GET /bear/bear_id - read specific bear, PUT ' \
       '/bear/bear_id - update specific bear, DELETE /bear - delete all bears, DELETE /bear/bear_id - ' \
       'delete specific bear. Example of ber json: {"bear_type":"BLACK","bear_name":"mikhail","bear_age":17.5}. ' \
       'Available types for bears are: POLAR, BROWN, BLACK and GUMMY.'

bears = []

bearss = {'bear_id': 0}


@app.route('/info', methods=['GET'])
def get_info():
    return info


@app.route('/bear', methods=['GET'])
def get_bears():
    return jsonify(bears)


@app.route('/bear/<int:bear_id>', methods=['GET'])
def get_bear(bear_id):
    bear = [bear for bear in bears if bear['bear_id'] == bear_id]
    if not bear:
        abort(HTTPStatus.NOT_FOUND)
    return jsonify(bear[0])


@app.route('/bear', methods=['POST'])
def create_bear():
    if request.json['bear_type'] not in ("POLAR", "BROWN", "BLACK", "GUMMY"):
        abort(HTTPStatus.BAD_REQUEST)
    if request.json['bear_age'] < 0:
        abort(HTTPStatus.BAD_REQUEST)
    bear = {
        'bear_id': bearss['bear_id'] + 1,
        'bear_type': request.json.get('bear_type', ""),
        'bear_name': request.json.get('bear_name', ""),
        'bear_age': request.json.get('bear_age', ''),
    }
    bearss['bear_id'] = bearss['bear_id'] + 1
    bears.append(bear)
    return jsonify(bear), HTTPStatus.CREATED


@app.route('/clear', methods=['POST'])
def clear_id():
    global bearss
    bearss = {'bear_id': 0}
    return "Bear id reset", HTTPStatus.OK


@app.route('/bear/<int:bear_id>', methods=['PUT'])
def update_bear(bear_id):
    bear = [bear for bear in bears if bear['bear_id'] == bear_id]
    if not bear:
        abort(HTTPStatus.NOT_FOUND)
    if not request.json:
        abort(HTTPStatus.BAD_REQUEST)
    if request.json['bear_type'] not in ("POLAR", "BROWN", "BLACK", "GUMMY"):
        abort(HTTPStatus.BAD_REQUEST)
    if request.json['bear_age'] < 0:
        abort(HTTPStatus.BAD_REQUEST)
    bear[0]['bear_type'] = request.json.get('bear_type', bear[0]['bear_type'])
    bear[0]['bear_name'] = request.json.get('bear_name', bear[0]['bear_name'])
    bear[0]['bear_age'] = request.json.get('bear_age', bear[0]['bear_age'])
    return "Bear successfully updated", HTTPStatus.OK


@app.route('/bear', methods=['DELETE'])
def delete_bears():
    bears.clear()
    return "Bears successfully deleted", HTTPStatus.OK


@app.route('/bear/<int:bear_id>', methods=['DELETE'])
def delete_bear(bear_id):
    global bears
    tmp_bears = [bear for bear in bears if bear['bear_id'] != bear_id]
    if len(tmp_bears) == len(bears):
        abort(HTTPStatus.NOT_FOUND)
    bears = tmp_bears
    return "Bear successfully deleted", HTTPStatus.OK


if __name__ == '__main__':
    app.run(host="0.0.0.0")

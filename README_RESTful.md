## Назначение
Данная веб-служба позволяет записывать, редактировать, удалять и просматривать информацию о медведях. 


## Докер 
Докер располагается по адресу ```https://hub.docker.com/r/deusvult1707/restful_bear```


## Запуск докера 
```docker run -p 5000:5000 -it deusvult1707/restful_bear:latest```


## Запуск веб-службы без докера
```python app.py```


## Пример записи 
```{"bear_id":id,"bear_type":"BLACK","bear_name":"mikhail","bear_age":17.5}```


## Доступные действия
### Просмотр списка всех медведей
**Команда**

```curl -X GET -i http://0.0.0.0:5000/bear```


### Просмотр информации о конкретном медведе
**Команда**

```curl -X GET -i http://0.0.0.0:5000/bear/2```


### Создание нового медведя
**Команда**

```curl -H "Content-Type: application/json" -X POST -i http://0.0.0.0:5000/bear -d '{"bear_age":10,"bear_name":"Mikhail","bear_type":"POLAR"}'```


### Изменение информации о конкретном медведе
**Команда**

```curl -H "Content-Type: application/json" -X PUT -i http://0.0.0.0:5000/bear/2 -d '{"bear_age":20,"bear_name":"Mickle","bear_type":"GUMMY"}'```


### Удаление информации о конкретном медведе
**Команда**

```curl -X DELETE -i http://0.0.0.0:5000/bear/1 ```


### Удаление информации медведях
**Команда**

```curl -X DELETE -i http://0.0.0.0:5000/bear```



































